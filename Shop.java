import java.util.Scanner;
public class Shop
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		Pizza [] pizzas = new Pizza[4];
		for (int i = 0; i<3; i++)
		{
			pizzas[i] = new Pizza();
			System.out.println("What size?");
			pizzas[i].size = sc.next();
			
			System.out.println("What topping?");
			pizzas[i].topping = sc.next();
			
			System.out.println("What type of meat?");
			pizzas[i].meat = sc.next();
		}
		//Printing the fields of the last product
		System.out.println();
		System.out.println(pizzas[2].size);
		System.out.println(pizzas[2].topping);
		System.out.println(pizzas[2].meat);
		
		//instance method
		pizzas[2].isHealthy();
	}
}